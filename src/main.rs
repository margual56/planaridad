use std::io::{self, Write};

pub fn readf() -> f32 {
    let mut n = String::new();

    io::stdin()
        .read_line(&mut n)
        .expect("failed to read input.");
    
        n.trim().parse::<f32>().expect("Invalid input")

}

pub fn main() {
    let N: f32;
    let M: f32;
    
    print!("N=");
    let _ = io::stdout().flush();
    N = readf();

    print!("M=");
    let _ = io::stdout().flush();
    M = readf();

    let R = -N+2.+M;

    println!("\n2+M-N = R --> R = {}", R);

    let A1 = R* 3./2.;
    let C1 = 3.*N-6.;

    let cond1 = A1 <= M && M <= C1 ;
    let res1 = if cond1 { "True" } else { "False" }; 
    println!("\n{} <= {} <= {} --> {}", A1, M, C1, res1);

    if !cond1 {
        std::process::exit(0);
    }

    println!("La condición se cumple, así que no está claro si es plano o no. Seguimos:\n");    

    print!("P=");
    let _ = io::stdout().flush();
    let P = readf();

    let A2 = P/2.0 * R;
    let C2 = P*(N-2.0)/(P-2.0);

    let cond2 = A2 <= M && M<= C2 ;
    let res2 = if cond2 { "True" } else { "False" }; 
    println!("{} <= {} <= {} --> {}", A2, M, C2, res2);
    println!("La condición se cumple, así que no podemos asegurar si es plano o no\n");   
}
